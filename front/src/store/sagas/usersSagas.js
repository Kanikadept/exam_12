import {put, takeEvery} from 'redux-saga/effects';
import axiosApi from "../../axiosApi";
import {NotificationManager} from 'react-notifications';
import {
  facebookLoginRequest,
  loginFailure, loginRequest,
  loginSuccess, logoutRequest,
  logoutSuccess,
  registerFailure,
  registerRequest,
  registerSuccess
} from "../actions/usersActions";
import {historyPush} from "../actions/historyActions";

export function* registerUser({payload: userData}) {
  try {
    const response = yield axiosApi.post('/users', userData);
    yield put(registerSuccess(response.data));
    yield put(historyPush('/'));
  } catch (error) {
    yield put(registerFailure(error.response.data));
  }
}

export function* loginUser({payload: userData}) {
  try {
    const response = yield axiosApi.post('/users/sessions', userData);
    yield put(loginSuccess(response.data.user));
    yield put(historyPush('/'));
    NotificationManager.success('Login successful');
  } catch (error) {
    yield put(loginFailure(error.response.data));
  }
}

export function* facebookLogin({payload: data}) {
  try {
    const response = yield axiosApi.post('/users/facebookLogin', data);
    yield put(loginSuccess(response.data.user));
    yield put(historyPush('/'));
    NotificationManager.success('Login successful');
  } catch (error) {
    yield put(loginFailure(error.response.data));
  }
}

export function* logout() {
  try {
    yield axiosApi.delete('/users/sessions');
    yield put(logoutSuccess());
    yield put(historyPush('/'));
    NotificationManager.success('Logged out!');
  } catch (e) {
    NotificationManager.error('Could not logout');
  }
}

const usersSagas = [
  takeEvery(registerRequest, registerUser),
  takeEvery(loginRequest, loginUser),
  takeEvery(facebookLoginRequest, facebookLogin),
  takeEvery(logoutRequest, logout),
];

export default usersSagas;