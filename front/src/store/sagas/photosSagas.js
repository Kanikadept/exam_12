import axiosApi from "../../axiosApi";
import {put, takeEvery} from "redux-saga/effects";
import {historyPush} from "../actions/historyActions";
import {NotificationManager} from "react-notifications";
import {
    createPhotoSuccess,
    deletePhotoSuccess,
    fetchPhotosSuccess, fetchUserPhotosRequest,
    fetchUserPhotosSuccess
} from "../actions/photosActions";
import {
    fetchPhotosRequest,
    createPhotoRequest,
    deletePhotoRequest,
} from "../actions/photosActions";


export function* fetchPhotos() {
    try {
        const photosResponse = yield axiosApi.get('/photos');
        yield put(fetchPhotosSuccess(photosResponse.data));
    } catch (err) {
        NotificationManager.error('Could not fetch photos');
    }
}

export function* fetchUserPhotos({payload: userId}) {
    try {
        const photosResponse = yield axiosApi.get('/photos/'+ userId);
        yield put(fetchUserPhotosSuccess(photosResponse.data));
    } catch (err) {
        NotificationManager.error('Could not fetch photos');
    }
}

export function* createPhoto({payload: photo}) {
    try {
        const photoResponse = yield axiosApi.post('/photos', photo);
        yield put(createPhotoSuccess(photoResponse.data));
        yield put(historyPush('/'));
        NotificationManager.success('photo was added!');
    } catch (err) {
        NotificationManager.error('Could not add photo');
    }
}

export function* deletePhoto({payload:photoId}) {
    try {
        console.log(photoId)
        yield axiosApi.delete('/photos/'+ photoId);
        yield put(deletePhotoSuccess());
        yield put(historyPush('/'));
        yield fetchPhotos();
        NotificationManager.success('Photo was deleted!');
    } catch (err) {
        NotificationManager.error('Could not delete photo');
    }
}

const photosSagas = [
    takeEvery(fetchPhotosRequest, fetchPhotos),
    takeEvery(createPhotoRequest, createPhoto),
    takeEvery(deletePhotoRequest, deletePhoto),
    takeEvery(fetchUserPhotosRequest, fetchUserPhotos),
];

export default photosSagas;