import {combineReducers} from "redux";
import usersSlice from "./slices/usersSlice";
import photosSlice from "./slices/photosSlice";

const rootReducer = combineReducers({
  users: usersSlice.reducer,
  photos: photosSlice.reducer
});

export default rootReducer;