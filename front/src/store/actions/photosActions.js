import photosSlice from "../slices/photosSlice";

export const {
    fetchPhotosSuccess,
    fetchPhotosRequest,
    fetchPhotosFailure,

    createPhotoSuccess,
    createPhotoRequest,
    createPhotoFailure,

    deletePhotoRequest,
    deletePhotoSuccess,
    deletePhotoFailure,

    fetchUserPhotosRequest,
    fetchUserPhotosSuccess,
    fetchUserPhotosFailure,
} = photosSlice.actions;