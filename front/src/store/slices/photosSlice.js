import {createSlice} from "@reduxjs/toolkit";

const initialState = {
    photos: [],
    photosLoading: false,
    photosError: false,
    createPhotoLoading: false,
    createPhotoError: false,

    deletePhotoLoading: false,
    deletePhotoFailure: null,

    userPhotos: [],
}

const name = "photos";

const photosSlice = createSlice({
    name,
    initialState,
    reducers: {
        fetchPhotosRequest: state => {
            state.photosLoading = true;
        },
        fetchPhotosSuccess: (state, {payload: photos}) => {
            state.photos = photos;
            state.photosLoading = false;
        },
        fetchPhotosFailure: (state, payload) => {
            state.photosError = payload;
            state.photosLoading = false;
        },
        fetchUserPhotosRequest: state => {
            state.photosLoading = true;
        },
        fetchUserPhotosSuccess: (state, {payload: photos}) => {
            state.userPhotos = photos;
        },
        fetchUserPhotosFailure: (state, payload) => {
            state.createPhotoError = payload;
            state.photosLoading = false;
        },
        /////////////////////CREATE PHOTOS
        createPhotoRequest: state => {
            state.createPhotoLoading = true;
        },
        createPhotoSuccess: state => {
            state.createPhotoLoading = false;
        },
        createPhotosFailure: (state, payload) => {
            state.createPhotoError = payload
            state.createPhotoLoading = false;
        },
        /////////////////////DELETE PHOTO
        deletePhotoRequest: state => {
            state.deletePhotoLoading = true;
        },
        deletePhotoSuccess: state => {
            state.deletePhotoLoading = false;
        },
        deletePhotoFailure: (state, payload) => {
            state.deletePhotoFailure = payload
            state.deletePhotoLoading = false;
        }
    }
});

export default photosSlice;