import {Route, Switch} from "react-router-dom";
import Layout from "./Components/Layout/Layout";
import LogIn from "./Containers/Login/LogIn";
import Register from "./Containers/Register/Register";
import './App.css';
import Gallery from "./Containers/Gallery/Gallery";
import UserPhotos from "./Containers/Gallery/UserPhotos/UserPhotos";
import PhotoForm from "./Containers/PhotoForm/PhotoForm";

const App = () => {


  return (
    <Layout>
      <Switch>
        <Route path="/" exact component={Gallery}/>
        <Route path="/register" exact component={Register}/>
        <Route path="/login" exact component={LogIn}/>
        <Route path="/users/:id" exact component={UserPhotos}/>
        <Route path="/addPhoto" exact component={PhotoForm}/>
      </Switch>
    </Layout>
  )
}


export default App;
