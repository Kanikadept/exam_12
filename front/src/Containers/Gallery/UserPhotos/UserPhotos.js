import React, {useEffect} from 'react';
import './UserPhotos.css';
import {useDispatch, useSelector} from "react-redux";
import Photo from "../Photo/Photo";
import {fetchUserPhotosRequest} from "../../../store/actions/photosActions";
import {NavLink} from "react-router-dom";

const UserPhotos = props => {
    const dispatch = useDispatch();
    const photos = useSelector(state => state.photos.userPhotos);
    const user = useSelector(state => state.users.user);

    let addButton = null;
    let userGallery = null;

    useEffect(() => {
        dispatch(fetchUserPhotosRequest(props.match.params.id));
    }, [dispatch, props.match.params.id]);

    if(photos.length > 0) {
        userGallery = <span>{photos[0].user.username}'s Gallery</span>;
    }

    if(user && (photos.length > 0)) {
        if(user._id === photos[0].user._id) {
            addButton = <NavLink to={"/addPhoto"}><button>Add photo</button></NavLink>;
        }
    }


    return (
        <>
            {userGallery && userGallery}
            {addButton && addButton}
            <div className="user-photos">
                {photos.map(photo => {
                    return <Photo key={photo._id}
                                  title={photo.title}
                                  image={photo.image}
                                  author={photo.user.username}
                    />
                })}
            </div>
        </>

    );
};

export default UserPhotos;