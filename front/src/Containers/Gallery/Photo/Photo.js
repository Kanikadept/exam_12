import React, {useState} from 'react';
import './Photo.css';
import {apiURL} from "../../../config";
import Modal from "../../../Components/Modal/Modal/Modal";
import {NavLink} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {deletePhotoRequest} from "../../../store/actions/photosActions";

const Photo = ({photoId, userId, title, image, author}) => {
    const dispatch = useDispatch();

    const user = useSelector(state => state.users.user);
    const [open, setOpen] = useState(false);
    let deleteButton = null;

    const openModel = () => {
        setOpen(true);
    }
    const closeModel = () => {
        setOpen(!open);
    }

    const handleDelete = () => {
        dispatch(deletePhotoRequest(photoId))
    }

    if(user) {
        if(userId === user._id) {
            deleteButton = <button onClick={handleDelete}>delete</button>
        }
    }
    
    return (
        <>
            <Modal show={open} closed={closeModel}><img className="modal-image" src={apiURL +'/'+ image} alt=""/></Modal>
            <div className="photo__box" >
                <div className="photo">
                    <div className="photo__wrapper" onClick={() => openModel()}>
                        <img src={apiURL +'/'+ image} alt=""/>
                    </div>
                    <span className="photo__title"><NavLink to={"/users/"+ userId}>{title}</NavLink></span>

                    <span className="photo__author"><strong>By: </strong>{author}</span>
                    {deleteButton && deleteButton}
                </div>

            </div>
        </>
    );
};

export default Photo;