import React, {useEffect} from 'react';
import './Gallery.css';
import {useDispatch, useSelector} from "react-redux";
import Photo from "./Photo/Photo";
import {fetchPhotosRequest} from "../../store/actions/photosActions";

const Gallery = () => {

    const dispatch = useDispatch();
    const photos = useSelector(state => state.photos.photos);


    useEffect(() => {
        dispatch(fetchPhotosRequest());
    }, [dispatch]);
    return (
        <div className="gallery">
            {photos.map(photo => {
                return <Photo key={photo._id}
                              photoId={photo._id}
                              userId={photo.user._id}
                              title={photo.title}
                              image={photo.image}
                              author={photo.user.username}/>
            })}
        </div>
    );
};

export default Gallery;