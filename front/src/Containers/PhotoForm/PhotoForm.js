import React, {useState} from 'react';
import {useDispatch} from "react-redux";
import {createPhotoRequest} from "../../store/actions/photosActions";
import FileInput from "./FileInput/FileInput";
import './PhotoForm.css'

const PhotoForm = () => {
    const dispatch = useDispatch();

    const [photo, setPhoto] = useState({
        title: '',
        image: '',
    });

    const handleChange = event => {
        const {name, value} = event.target;
        setPhoto(prev => ({...prev, [name] : value}));
    }

    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];

        setPhoto(prevSate => ({
            ...prevSate,
            [name]: file
        }));
    };


    const handleSubmit = (event) => {
        event.preventDefault();
        const formData = new FormData();
        Object.keys(photo).forEach(key => {
            formData.append(key, photo[key]);
        });
        dispatch(createPhotoRequest(formData));
    }

    return (
        <form onSubmit={handleSubmit} className="photo-form">
            <strong>Add new photo</strong>
            <div className="photo-form__row">
                <label><b>Title:</b></label>
                <input onChange={handleChange} type="text" name="title" value={photo.title} required/>
            </div>
            <div className="photo-form__row">
                <label><b>Image:</b></label>
                <FileInput name="image" label="Image" onChange={fileChangeHandler}/>
            </div>
            <button>Add</button>
        </form>
    );
};

export default PhotoForm;