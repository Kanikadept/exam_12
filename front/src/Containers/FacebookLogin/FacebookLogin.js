import React from 'react';
import FacebookLoginButton from 'react-facebook-login/dist/facebook-login-render-props';
import {useDispatch} from "react-redux";
import {facebookLoginRequest} from "../../store/actions/usersActions";

const FacebookLogin = () => {

    const dispatch = useDispatch();

    const facebookResponse = response => {
       if (response.id) {
           dispatch(facebookLoginRequest(response));
       }
    }

    return (
        <FacebookLoginButton
            appId="736938163638144"
            fields="name,email,picture"
            render={props => (
                <button
                    onClick={props.onClick}
                >
                    Login with Facebook
                </button>
            )}
            callback={facebookResponse}
        />
    );
};

export default FacebookLogin;