import React from 'react';
import {NavLink} from "react-router-dom";

const AnonymousMenu = () => {
    return (
        <>
            <NavLink to="/register"><button className="header__btn__sign">Register</button></NavLink>
            <NavLink to="/login"><button className="header__btn__log">Login</button></NavLink>
        </>
    );
};

export default AnonymousMenu;