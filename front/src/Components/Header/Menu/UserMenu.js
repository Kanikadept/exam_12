import React from 'react';

import './UserMenu.css';
import {useDispatch} from "react-redux";
import {logoutSuccess} from "../../../store/actions/usersActions";
import {NavLink} from "react-router-dom";

const UserMenu = ({user}) => {

    const dispatch = useDispatch();

    const handleLogout = () => {
        dispatch(logoutSuccess());
    }

    return (
        <div className="user-menu">
            <span>Hello,  <NavLink to={"/users/" + user._id}>{user.username}!</NavLink></span>
            <button onClick={handleLogout}>Logout</button>
        </div>
    );
};

export default UserMenu;