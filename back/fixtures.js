const mongoose = require('mongoose');
const config = require('./config');
const User = require("./models/User");
const Photo = require("./models/Photo");
const {nanoid} = require('nanoid');

const run = async () => {
    await mongoose.connect(config.db.url, config.db.options);

    const collections = await mongoose.connection.db.listCollections().toArray();

    for (const coll of collections) {
        await mongoose.connection.db.dropCollection(coll.name);
    }

    const [user1, user2] = await User.create({
        email: 'some@gmail.com',
        password: '123',
        token: nanoid(),
        role: 'user',
        username: 'Byun'
    }, {
        email: 'other@gmail.com',
        password: '123',
        token: nanoid(),
        role: 'user',
        username: 'Parting'
    });

    const [flat, rainbow] = await Photo.create({
        title: 'flat',
        image: 'fixtures/fang.jpg',
        user: user1,
    }, {
        title: 'rainbow',
        image: 'fixtures/hibana.jpg',
        user: user2,
    });

    await mongoose.connection.close();
};

run().catch(console.error);