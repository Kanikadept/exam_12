const express = require('express');

const auth = require("../middleware/auth");
const Photo = require("../models/Photo");
const upload = require('../multer').images;

const router = express.Router();

router.post('/', auth, upload.single('image'), async (req, res) => {
    try {
        const photoData = {
            title: req.body.title,
            user: req['user'],
        }

        if (req.file) {
            photoData.image = req.file.filename;
        }

        const photo = new Photo(photoData);
        const photoResponse = await photo.save();

        return res.send(photoResponse);

    } catch (err) {
        return res.status(400).send(err);
    }
});

router.get('/', async (req, res) => {
    try {
        const photoResponse = await Photo.find().populate('user', 'username');
        return res.send(photoResponse);

    } catch (err) {
        return res.status(400).send(err);
    }
});

router.get('/:id', async (req, res) => {
    try {
        const photoResponse = await Photo.find({user: req.params.id}).populate('user', 'username');
        return res.send(photoResponse);

    } catch (err) {
        return res.status(400).send(err);
    }
});

router.delete('/:id', auth, async (req, res) => {
    try {
        const photoResponse = await Photo.findOneAndDelete({_id: req.params.id, user: req['user']});
        return res.send(photoResponse);

    } catch (err) {
        return res.status(400).send(err);
    }
});

module.exports = router;