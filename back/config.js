const path = require('path');
const rootPath = __dirname;

module.exports = {
  rootPath,
  uploadPath: path.join(rootPath, 'public'),
  db: {
    url: 'mongodb://localhost/gallery',
    options: {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    },
  },
  facebook: {
    appId: '736938163638144',
    appSecret: 'a310164ad7cbbc31206c2248f0579012'
  },
};